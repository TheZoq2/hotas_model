use scad::*;
use scad_util::constants::{x_axis, y_axis, z_axis};

use nalgebra::Vector3;

use crate::RAIL_SEPARATION;
pub const SIDE_PADDING: f32 = 2.;
pub const BEARING_DIAMETER: f32 = 15.;
pub const BEARING_LENGTH: f32 = 24.;
pub const TRAVLER_WIDTH: f32 = RAIL_SEPARATION + BEARING_DIAMETER + SIDE_PADDING*2.;
pub const TRAVLER_LENGTH: f32 = BEARING_LENGTH + SIDE_PADDING;
pub const BACK_BLOCK_WIDTH: f32 = 6.;

pub const DOVETAIL_X: f32 = TRAVLER_LENGTH;
pub const DOVETAIL_Y: f32 = TRAVLER_LENGTH;
pub const DOVETAIL_Z: f32 = 7.;

fn throttle_mount_dovetail(outer_size: Vector3<f32>) -> ScadObject {
    let points = vec![
        vec2(outer_size.y/2., outer_size.z),
        vec2(-outer_size.y/2., outer_size.z),
        vec2(-(outer_size.y/2. - outer_size.z), 0.),
        vec2(outer_size.y/2. - outer_size.z, 0.),
    ];

    let eparams = LinExtrudeParams{
        height: outer_size.x,
        center: true,
        .. Default::default()
    };

    scad!(Rotate(90., x_axis()); {
        scad!(LinearExtrude(eparams); {
            scad!(Polygon(PolygonParameters::new(points)))
        })
    })
}


pub fn object_at_travler_screwhole(block_size: Vector3<f32>, object: ScadObject) -> ScadObject {
    scad!(Translate(-y_axis() * block_size.y / 4. + z_axis() * (block_size.z/2.)); {
        object
    })
}

pub fn throttle_rail_travler_outer(block_size: Vector3<f32>) -> ScadObject {
    let block = centered_cube(block_size, (true, true, true));

    let top_mount_cutters = {
        let size = vec3(3., block_size.y, 3.);

        let shape = centered_cube(size, (true, true, true));

        let x_offset = block_size.x/2. - size.x / 2.;
        let z_offset = block_size.z/2. - size.z / 2.;

        scad!(Union; {
            scad!(Translate(vec3(x_offset, 0., z_offset)); shape.clone()),
            scad!(Translate(vec3(-x_offset, 0., z_offset)); shape.clone()),
        })
    };

    scad!(Difference; {
        scad!(Union; {
            block,
        }),
        top_mount_cutters,
    })
}

pub fn throttle_rail_travler(is_front: bool) -> ScadObject {
    let bearing_diameter = BEARING_DIAMETER;
    let bearing_len = BEARING_LENGTH;
    let bearing_padding = 0.5;
    let rail_separation = RAIL_SEPARATION;
    let rail_diameter = 8.;
    let side_padding = SIDE_PADDING;

    let block_size = vec3(
            TRAVLER_WIDTH,
            TRAVLER_LENGTH,
            bearing_diameter + side_padding * 2.,
        );
    let dovetail_size = vec3(DOVETAIL_X, DOVETAIL_Y, DOVETAIL_Z);


    let back_block_width = BACK_BLOCK_WIDTH;
    let back_block = {
        let shape = centered_cube(
            vec3(block_size.x, back_block_width, block_size.z + dovetail_size.z),
            (true, false, false)
        );

        scad!(Translate(vec3(0., block_size.y / 2. - back_block_width, - block_size.z/2.));{
            shape
        })
    };

    let bearing_cutout_shape = {
        let bearing_shape = centered_cylinder(
            bearing_len,
            Diameter(bearing_diameter + bearing_padding)
        );
        let rail_shape = centered_cylinder(
            block_size.x,
            Diameter(rail_diameter + bearing_padding)
        );

        scad!(Rotate(90., x_axis()); {
            bearing_shape,
            rail_shape,
        })
    };

    let bearings = {
        scad!(Union; {
            scad!(Translate(x_axis() * rail_separation / 2.); bearing_cutout_shape.clone()),
            scad!(Translate(-x_axis() * rail_separation / 2.); bearing_cutout_shape.clone())
        })
    };

    let separation_distance = block_size.y / 2. - back_block_width;

    let nut_distance = 4.5;
    // Screwhole to connect the pieces
    let screwhole = {
        let diameter = 4.;
        let head_diameter = 6.; 

        let depth = 20.;

        let screwhole = scad!(Cylinder(depth *2., Diameter(diameter)));
        let head = scad!(Translate(depth * z_axis()); {
            scad!(Cylinder(depth, Diameter(head_diameter)))
        });

        scad!(Translate(-(depth - nut_distance / 2.) * z_axis()); {
            screwhole,
            head,
        })
    };

    let screwhole_with_nut = {
        let nut_diameter = 5.5;
        let nut_major_diameter = 6.5;
        let nut_height = 2.6;
        let nut_hole = centered_cube(vec3(nut_diameter, 100., nut_height), (true, false, false));

        scad!(Union; {
            screwhole.clone(),
            scad!(Translate(vec3(0., -nut_major_diameter/2., -nut_distance)); {
                nut_hole
            })
        })
    };

    /*
    let dovetail = scad!(Translate(z_axis() * block_size.z/2.); {
        throttle_mount_dovetail(dovetail_size)
    });
    */

    let pot_pin_hole = {
        let shape = centered_cylinder(block_size.z, Diameter(2.5));
        scad!(Mirror(z_axis()); shape)
    };

    let full_shape = scad!(Difference; {
        throttle_rail_travler_outer(block_size),
        bearings,
        scad!(Translate(vec3(0., separation_distance, -block_size.z * (5./16.))); {
            scad!(Rotate(-90., x_axis()); screwhole_with_nut.clone()),
        }),
        scad!(Translate(vec3(0., separation_distance, block_size.z * (5./16.))); {
            scad!(Mirror(z_axis()); {
                scad!(Rotate(-90., x_axis()); screwhole_with_nut),
            })
        }),
        object_at_travler_screwhole(block_size, scad!(Rotate(180., x_axis()); screwhole)),
        pot_pin_hole,
    });

    if is_front {
        scad!(Difference; {
            full_shape,
            back_block,
        })
    }
    else {
        scad!(Intersection; {
            full_shape,
            back_block,
        })
    }
}

pub fn throttle_rail_travler_cutout(padding: f32) -> ScadObject {
    let bearing_diameter = BEARING_DIAMETER;
    let side_padding = SIDE_PADDING;

    let block_size = vec3(
            TRAVLER_WIDTH,
            TRAVLER_LENGTH,
            bearing_diameter + side_padding * 2.,
        );

    let screwhole = scad!(Cylinder(100., Diameter(2.5)));

    let nut_hole = scad!(Translate(4. * z_axis()); {
        scad_util::nut(6., 100.)
    });

    scad!(Union; {
        scad!(Minkowski; {
            throttle_rail_travler_outer(block_size),
            centered_cube(vec3(padding, padding, padding), (true, true, true))
        }),
        object_at_travler_screwhole(block_size, screwhole),
        object_at_travler_screwhole(block_size, nut_hole),
    })
}

pub fn rudder_mount(is_front: bool) -> ScadObject {
    let length = TRAVLER_LENGTH;
    let dovetail_extra = 1.5;
    let height_over_dovetail = 4.;
    let rudder_bearing_diameter = 9.1;
    // NOTE not tight bound
    let rudder_axle_diameter = 7.;
    let rudder_axle_center_length = 16.5;
    let bearing_length = 2.;
    let rudder_mount_height = 20.;

    let base_points = vec![
        vec2(-TRAVLER_WIDTH/2., 0.),
        vec2(-TRAVLER_WIDTH/2., DOVETAIL_Z),
        vec2(-TRAVLER_WIDTH/2. + height_over_dovetail, DOVETAIL_Z + height_over_dovetail),
        vec2(TRAVLER_WIDTH/2. - height_over_dovetail, DOVETAIL_Z + height_over_dovetail),
        vec2(TRAVLER_WIDTH/2., DOVETAIL_Z),
        vec2(TRAVLER_WIDTH/2., 0.),
    ];

    let shape = scad!(Polygon(PolygonParameters::new(base_points)));

    let extrude_params = LinExtrudeParams{
        height: length,
        center: true,
        .. Default::default()
    };

    let travler_shape = scad!(Difference; {
        scad!(Rotate(90., x_axis()); {
            scad!(LinearExtrude(extrude_params); shape),
        }),
    });

    let dovetail_shape = throttle_mount_dovetail(vec3(
            length,
            DOVETAIL_X + dovetail_extra,
            DOVETAIL_Z - dovetail_extra
        ));

    let rudder_axle_cutout = {
        scad!(Rotate(90., x_axis()); {
            centered_cylinder(
                rudder_axle_center_length + bearing_length*2.,
                Diameter(rudder_bearing_diameter)
            ),
            centered_cylinder(
                100.,
                Diameter(rudder_axle_diameter)
            ),
        })
    };

    let rudder_holder = {
        let diameter = rudder_bearing_diameter + 4.;
        let pillar = centered_cube(
            vec3(diameter, length, rudder_mount_height),
            (true, true, false)
        );

        scad!(Difference; {
            scad!(Union; {
                scad!(Translate(rudder_mount_height * z_axis()); {
                    scad!(Rotate(90., x_axis()); centered_cylinder(length, Diameter(diameter))),
                }),
                pillar
            }),
            centered_cube(vec3(100., rudder_axle_center_length, 100.), (true, true, false))
        })
    };

    let screwholes = {
        let diameter = 2.5;
        let head_diameter = 6.;
        let head_shape = scad!(Translate(vec3(0., 0., 3.)); {
            scad!(Cylinder(length, Diameter(head_diameter)))
        });
        let shape = scad!(Union; {
            centered_cylinder(length, Diameter(diameter)),
            scad!(Hull; {
                head_shape.clone(),
                scad!(Translate(vec3(10., -10., 0.)); head_shape),
            })
        });

        let translated = scad!(Translate(vec3(TRAVLER_WIDTH/2. - head_diameter/2., 0.,  head_diameter/2.)); {
            scad!(Rotate(90., x_axis()); shape)
        });

        scad!(Union; {
            scad!(Mirror(x_axis()); translated.clone()),
            translated
        })
    };

    let back_block_cutout = {
        let shape = centered_cube(vec3(TRAVLER_WIDTH, BACK_BLOCK_WIDTH, DOVETAIL_Z), (true, false, false));
        scad!(Translate((length/2. - BACK_BLOCK_WIDTH) * y_axis()); shape)
    };



    let shape = scad!(Difference; {
        scad!(Union; {
            travler_shape,
            rudder_holder,
        }),
        scad!(Translate(rudder_mount_height * z_axis()); rudder_axle_cutout),
        dovetail_shape,
        screwholes,
        back_block_cutout
    });

    let front_cutout = centered_cube(vec3(100., 100., 100.), (true, false, true));
    if is_front {
        scad!(Difference; {shape, front_cutout})
    }
    else {
        scad!(Intersection; {shape, front_cutout})
    }
}

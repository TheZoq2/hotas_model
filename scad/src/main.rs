use scad::*;
use scad_util::constants::{x_axis, y_axis, z_axis};

pub const RAIL_SEPARATION: f32 = 20.;

mod base_plate;
mod travler;

fn secondary_throttle_pot_mount() -> ScadObject {
    let outer_shape = vec3(20., 50., 20.8);
    let base = centered_cube(outer_shape, (false, false, false));

    let pot_cutout = {
        let hole_diameter = 6.9;
        let y_offset = 37.5;
        let tab_y_offset = -4.3 - hole_diameter / 2.;
        let hole_shape = scad!(Cylinder(100., Diameter(hole_diameter)));
        let tab_shape = scad!(Translate(vec3(0., tab_y_offset, 0.)); {
            centered_cube(vec3(3., 3., 10.), (true, true, false)),
        });
        let shape = scad!(Union; {
            hole_shape,
            tab_shape
        });
        let rotated = scad!(Rotate(90., y_axis()); shape);
        scad!(Translate(vec3(0., y_offset, outer_shape.z/2.)); rotated)
    };

    scad!(Difference; {
        base,
        pot_cutout
    })
}

pub const FLEX_TUBE_DIAMETER: f32 = 10.;

fn rail_mount_flex_tube(outer_diameter: f32, rail_diameter: f32) -> ScadObject {
    let length = 10.;

    scad!(Difference; {
        scad!(Cylinder(length, Diameter(outer_diameter))),
        scad!(Cylinder(length, Diameter(rail_diameter)))
    })
}

#[allow(dead_code)]
fn flex_tube_assortment() -> ScadObject {
    let diameters = [
        (10., 8.),
        (9.5, 8.),
        (12., 8.),
        (12.5, 8.),
        (14.5, 8.),
        (10., 8.2),
        (9.5, 8.2),
        (12., 8.2),
        (14.5, 8.2)
    ];

    let mut result = scad!(Union);
    for (i, (outer, inner)) in diameters.iter().enumerate() {
        let pos = vec3(i as f32 % 3., (i as f32 / 3.).floor(), 0.) * 20.;

        result.add_child(scad!(Translate(pos); rail_mount_flex_tube(*outer, *inner)));
    }

    result
}

fn rail_mount_foot(thickness: f32, width: f32, screw_diameter: f32) -> ScadObject {
    let screw_distance = RAIL_SEPARATION;
    let place_at_screwholes = |x: ScadObject| {
        scad!(Union; {
            scad!(Translate(screw_distance/2. * x_axis()); x.clone()),
            scad!(Translate(-screw_distance/2. * x_axis()); x)
        })
    };
    scad!(Difference; {
        scad!(Hull; place_at_screwholes(scad!(Cylinder(thickness, Diameter(width))))),
        place_at_screwholes(scad!(Cylinder(thickness, Diameter(screw_diameter))))
    })
}

fn throttle_rail_mount() -> ScadObject {
    let rail_height = 20.; 
    let rail_separation = RAIL_SEPARATION;
    let washer_diameter = FLEX_TUBE_DIAMETER;
    let mount_radius = washer_diameter + 4.;
    let length = 10.;
    let base_width = 10.;
    let foot_thickness = 4.;
    let foot_screw_diameter = 2.5;

    let place_at_rail = |x: ScadObject| {
        scad!(Union; {
            scad!(Translate(rail_separation/2. * x_axis()); x.clone()),
            scad!(Translate(-rail_separation/2. * x_axis()); x)
        })
    };

    let outer_shape = scad!(Hull; {
        place_at_rail(scad!(Cylinder(length, Diameter(mount_radius))))
    });

    let base_leg = centered_cube(vec3(base_width, rail_height, length), (true, false, false));

    let base_foot = {
        let shape = rail_mount_foot(foot_thickness, length, 2.5);
        scad!(Translate(vec3(0., rail_height, length/2.)); {
            scad!(Rotate(90., x_axis()); shape)
        })
    };

    let back_stop_screwhole = scad!(Cylinder(length, Diameter(foot_screw_diameter)));

    scad!(Difference; {
        scad!(Union; {
            outer_shape,
            base_leg,
            base_foot
        }),
        place_at_rail(scad!(Cylinder(length, Diameter(washer_diameter)))),
        back_stop_screwhole
    })
}


fn write_to_file(filename: &str, obj: ScadObject) {
    let mut file = ScadFile::new();

    file.add_object(obj);

    file.set_detail(100);
    
    file.write_to_file(filename.into());
}

fn main() {
    write_to_file("secondary_throttle_pot_mount.scad", secondary_throttle_pot_mount());
    write_to_file("throttle_rail_travler_front.scad", travler::throttle_rail_travler(true));
    write_to_file("throttle_rail_travler_back.scad", travler::throttle_rail_travler(false));
    write_to_file("throttle_rail_travler_cutout.scad", travler::throttle_rail_travler_cutout(0.25));
    write_to_file("throttle_rail_mount.scad", throttle_rail_mount());
    write_to_file("flex_tube.scad", rail_mount_flex_tube(9.5, 8.2));
    write_to_file("flex_tube_thicker.scad", rail_mount_flex_tube(10., 8.2));
    write_to_file("rail_mount_foot.scad", rail_mount_foot(2., 8., 3.5));
    write_to_file("rudder_mount_front.scad", travler::rudder_mount(true));
    write_to_file("rudder_mount_back.scad", travler::rudder_mount(false));
}
